import {Component, EventEmitter, Output} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent {
  //daca trimitem informatii de la parinte catre copii: am folosit @Input in event-form
  @Output()//aici trimitem informatii de la copil catre parinte!
  actionConfirmedEvent = new EventEmitter<boolean>
  constructor(public dialogReference: MatDialogRef<ConfirmationDialogComponent>) {
  }
  confirm(){
    this.actionConfirmedEvent.emit(true);
  }


}
