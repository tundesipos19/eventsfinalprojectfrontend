import {Component} from '@angular/core';
import {Event} from "../model/event";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmationDialogComponent} from "../confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css']
})
export class EventPageComponent {
  event: Event = new Event(
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null);
  updateEventUrl: string = "";

  httpClient: HttpClient;
  route: ActivatedRoute;

  constructor(httpClient: HttpClient, route: ActivatedRoute, private router: Router, private dialog:MatDialog) {
    //daca declar un field privat in constructor, nu mai trebuie sa - l declar(in clasa), nu trebuie sa-l asignez(in constructor)
    this.httpClient = httpClient;
    this.route = route;
  }

  ngOnInit() {
    const eventId = this.route.snapshot.paramMap.get("id");//sintaxa pentru a accesa parametrul din url
    this.updateEventUrl = '/update-event/' +eventId;
    //accesam evenimentul cu id-ul 1 - response e obiectul ce-l primim noi
    this.httpClient.get("/api/events/" + eventId).subscribe(
      (response) => {
        console.log(response)
        this.event = response as Event;//convertim response in Event
        /*this.event.startDate = new Date(this.event.startDate);
        this.event.endDate = new Date(this.event.endDate);*/
        if (this.event.startDate != null)
          this.event.startDate = new Date(this.event.startDate!);
        if (this.event.endDate != null)
          this.event.endDate = new Date(this.event.endDate!);
      },
      (error) => {
        console.log(error);
        if (error.error == "There is no event with Id " + eventId) {
          //mesajul trebuie sa fie identic  cu mesajul din consola(inspect)- care vine defapt din EventService din readEvent
          this.router.navigate(["not-found"])
        }

      });

    //.subsribe()-are 2 parametrii: primul- ce se intampla daca totul e ok,al doilea, daca avem o eroare
    //.subscribe(() => {}, () => {})

  }
  public openConfirmationDialog(){
    var dialogRef = this.dialog.open(ConfirmationDialogComponent)//deschide componenta cu dialog!
    dialogRef.componentInstance.actionConfirmedEvent.subscribe(actionConfirmed => {
      if(actionConfirmed === true){//verificam, daca chiar actionConfirmed are valoarea true
        this.httpClient.delete('/api/events/' + this.event.id).subscribe(
          success =>{
            this.router.navigate(["/events"]);
          },
          error => {
            alert("Something went wrong!");
          }
        )
      }
    })//referinta face referire la valoarea instantei din clasa ConfirmationDialogComponent
  }
}
