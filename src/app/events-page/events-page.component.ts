import {Component} from '@angular/core';
import {Event} from "../model/event";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Account} from "../model/account";


@Component({
  selector: 'app-events-page',
  templateUrl: './events-page.component.html',
  styleUrls: ['./events-page.component.css']
})
export class EventsPageComponent {
  events: Event[] = [];

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  ngOnInit() {
    this.httpClient.get("/api/events").subscribe(
      (response) => {
        console.log(response);
        this.events = response as Event[];
      })
  }

  navigateToEvent(e: Event) {
    this.router.navigate(["/events/" + e.id]);
  }

}
