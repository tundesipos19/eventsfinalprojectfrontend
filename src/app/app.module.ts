import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {AppRoutingModule} from './app-routing.module';
import {MatNativeDateModule} from "@angular/material/core";

import {AppComponent} from './app.component';
import {EventPageComponent} from './event-page/event-page.component';
import {HttpClientModule} from "@angular/common/http";
import {NotFoundPageComponent} from './not-found-page/not-found-page.component';
import {CategoryPageComponent} from './category-page/category-page.component';
import {HeaderComponent} from './header/header.component';
import {EventsPageComponent} from './events-page/events-page.component';
import {EventCardComponent} from './event-card/event-card.component';
import {NewEventPageComponent} from './new-event-page/new-event-page.component';
import {EventFormComponent} from './event-form/event-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CategoriesPageComponent} from './categories-page/categories-page.component';
import {CategoryFormComponent} from './category-form/category-form.component';
import {CategoryCardComponent} from './category-card/category-card.component';
import {UpdateEventPageComponent} from './update-event-page/update-event-page.component';
import {NewCategoryPageComponent} from './new-category-page/new-category-page.component';
import {UpdateCategoryPageComponent} from './update-category-page/update-category-page.component';
import {AccountsPageComponent} from './accounts-page/accounts-page.component';
import {AccountPageComponent} from './account-page/account-page.component';
import {AccountFormComponent} from './account-form/account-form.component';
import {NewAccountComponent} from './new-account/new-account.component';
import {UpdateAccountComponent} from './update-account/update-account.component';
import {AccountCardComponent} from './account-card/account-card.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';
import {MatSelectModule} from "@angular/material/select";


@NgModule({
  declarations: [
    AppComponent,
    EventPageComponent,
    NotFoundPageComponent,
    CategoryPageComponent,
    HeaderComponent,
    EventsPageComponent,
    EventCardComponent,
    NewEventPageComponent,
    EventFormComponent,
    CategoriesPageComponent,
    CategoryFormComponent,
    CategoryCardComponent,
    UpdateEventPageComponent,
    NewCategoryPageComponent,
    UpdateCategoryPageComponent,
    AccountsPageComponent,
    AccountPageComponent,
    AccountFormComponent,
    NewAccountComponent,
    UpdateAccountComponent,
    AccountCardComponent,
    ConfirmationDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
