import {Component} from '@angular/core';

import {Category} from "../model/category";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.css']
})
export class CategoriesPageComponent {
  categories: Category[] = [];

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  ngOnInit() {
    this.httpClient.get("/api/categories").subscribe(
      (response) => {
        console.log(response);
        this.categories = response as Category[];
      }
    )
  }

  navigateToCategory(c: Category) {
    this.router.navigate(["/categories/" + c.id]);
  }
}
