import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {Event} from "../model/event";

@Component({
  selector: 'app-update-event-page',
  templateUrl: './update-event-page.component.html',
  styleUrls: ['./update-event-page.component.css']
})
export class UpdateEventPageComponent {
  //http://localhost:4200/update-event/5

  updateEvent: Event | null =null;

  constructor(
    private activatedRoute: ActivatedRoute, private httpClient: HttpClient) {
  }

  ngOnInit() {
    var eventId = this.activatedRoute.snapshot.paramMap.get('id');//eventId ia valoarea din url
    this.httpClient.get("/api/events/" + eventId).subscribe(
      (response) => {
        this.updateEvent = response as Event;
        if (this.updateEvent.startDate != null)
          this.updateEvent.startDate = new Date(this.updateEvent.startDate!);//cu ! se asigura ca startDate nu este null
        if (this.updateEvent.endDate != null)
          this.updateEvent.endDate = new Date(this.updateEvent.endDate);
      }
    )

  }

}
