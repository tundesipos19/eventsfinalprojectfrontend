import {Component, Input} from '@angular/core';
import {Account} from "../model/account";

@Component({
  selector: 'app-account-card',
  templateUrl: './account-card.component.html',
  styleUrls: ['./account-card.component.css']
})

export class AccountCardComponent {
  @Input()
  account: Account = new Account(
    1,
    "Sipos ",
    "Tunde",
    "me@gmail.com",
    "Abc_123",
    "Abc_123"
    );
}
