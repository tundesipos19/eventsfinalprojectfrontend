import { Component } from '@angular/core';
import {Category} from "../model/category";
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-update-category-page',
  templateUrl: './update-category-page.component.html',
  styleUrls: ['./update-category-page.component.css']
})
export class UpdateCategoryPageComponent {
  updateCategory: Category | null = null;
  constructor(private route: ActivatedRoute, private httpClient: HttpClient) {
  }
  ngOnInit(){
    var categoryId:string|null = this.route.snapshot.paramMap.get('id');
    this.httpClient.get("/api/categories/"+ categoryId).subscribe(
      (response) => {
        this.updateCategory = response as Category;

      }
    )
  }

}
