import { Component } from '@angular/core';
import {Account} from "../model/account";

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css']
})
export class NewAccountComponent {
  newAccount: Account={
    id: null,
    email: "",
    firstName: "",
    lastName: "",
    password: "",
    reTypePassword: ""
  }

}
