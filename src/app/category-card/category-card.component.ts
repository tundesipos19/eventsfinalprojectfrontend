import {Component, Input} from '@angular/core';
import {Category} from "src/app/model/category";

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.css']
})
export class CategoryCardComponent {
  @Input()
  category: Category = new Category(
    1,
    "Business Meetings, and Conferences: ",
    "These includes business-related meetings and conferences. It can be a meeting between the shareholders, employees, meeting for product launch, etc.",
    null
  );
}
