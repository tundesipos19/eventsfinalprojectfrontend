import {Component, Input} from '@angular/core';
import {Event} from "../model/event";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Category} from "../model/category";

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css']
})
export class EventFormComponent {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  @Input()//formul va primi input
  event: Event = {
    id: null,
    name: "",
    description: "",
    location: "",
    startDate: null,
    endDate: null,
    imgURL: null,
    category: null
  }
  categories:Category[] = [];

  //un container pentru toate casutele de input
  eventForm: FormGroup = new FormGroup({
    nameInput: new FormControl(),
    descriptionInput: new FormControl(),
    locationInput: new FormControl(),
    imgURLInput: new FormControl(),
    startDateInput: new FormControl(),
    endDateInput: new FormControl(),
    categoryInput: new FormControl()
  });

  ngOnInit() {
    console.log(this.event);
    this.eventForm = new FormGroup({
      nameInput: new FormControl(this.event.name),
      descriptionInput: new FormControl(this.event.description),
      locationInput: new FormControl(this.event.location),
      imgURLInput: new FormControl(this.event.imgURL),
      startDateInput: new FormControl(this.event.startDate),
      endDateInput: new FormControl(this.event.endDate),
      categoryInput: new FormControl(this.event.category)
    });
    this.getCategories();

  }
  getCategories(){
    this.httpClient.get("/api/categories").subscribe(
      (response) => {
        console.log(response);
        this.categories = response as Category[];
      }
    )
  }

  saveOrUpdateEvent() {
    /* alert("Save button pressed!");
     console.log(this.eventForm);
     this.populateEventFromForm();
     console.log(this.event);
     JSON.stringify(this.event);
     console.log(JSON.stringify(this.event));*/
    this.populateEventFromForm();
    if (this.event.id == null) {
      this.httpClient.post("/api/events", this.event).subscribe(
        (response) => {
          var savedEvent = response as Event;
          this.router.navigate(["/events/" + savedEvent.id]);
        });
    }
    if (this.event.id != null) {
      this.httpClient.put('/api/events/' + this.event.id, this.event).subscribe(
        (response) => {
          this.router.navigate(['/events/' + this.event.id]);
        }
      )
    }

  }

  populateEventFromForm() {
    this.event.name = this.eventForm.value.nameInput;
    this.event.description = this.eventForm.value.descriptionInput;
    this.event.location = this.eventForm.value.locationInput;
    this.event.startDate = this.eventForm.value.startDateInput;
    this.event.endDate = this.eventForm.value.endDateInput;
    this.event.imgURL = this.eventForm.value.imgURLInput;
    this.event.category = this.eventForm.value.categoryInput;
  }
}
