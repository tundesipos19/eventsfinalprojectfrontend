import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Account} from "../model/account";


@Injectable({//decorator
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpClient:HttpClient) {

  }

  public login(email: string | null, password: string | null) {
    let body = {
      "email": email,
      "password": password
    }

    return  this.httpClient.post('/api/authentication/login/', body);
  }

  public register(email:string,firstName: string, lastName: string, password:string, reTypePassword:string) {
    let body = {
      "email": email,
      "firstName": firstName,
      "lastName": lastName,
      "password": password,
      "reTypePassword": reTypePassword
    }
    return  this.httpClient.post('/api/authentication/register', body);
  }


}
