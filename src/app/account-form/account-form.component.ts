import {Component, Input} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {AccountService} from "./account-service";
import {Account} from "../model/account";


@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent {
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  firstNameFormControl = new FormControl('', [Validators.required]);
  lastNameFormControl = new FormControl('', [Validators.required]);
  passwordFormControl = new FormControl('', [Validators.required]);
  reTypePasswordFormControl = new FormControl('', [Validators.required]);

  viewType: string = "login";

  constructor(private httpClient: HttpClient, private router: Router, private accountService: AccountService) {
  }

  @Input()
  account: Account = {
    id: null,
    email: "",
    firstName: "",
    lastName: "",
    password: "",
    reTypePassword: ""
  }

  onViewChange() {
    if (this.viewType == 'login') {
      this.viewType = 'register';
    } else {
      this.viewType = 'login';
    }
  }

  onLogIn() {
    let email = this.emailFormControl.getRawValue()!;
    let password = this.passwordFormControl.getRawValue()!;
    this.accountService.login(email, password).subscribe((response: any) => {
      console.log(response);
      alert(response.message);
      this.router.navigate(["/events"])
    })
  }

  public onRegister() {
    let email = this.emailFormControl.getRawValue()!;
    let firstName = this.firstNameFormControl.getRawValue()!;
    let lastName = this.lastNameFormControl.getRawValue()!;
    let password = this.passwordFormControl.getRawValue()!;
    let reTypePassword = this.reTypePasswordFormControl.getRawValue()!;

    console.log(email +" " + firstName +" " + lastName +" " + password +" " + reTypePassword);
    if (password == reTypePassword) {
      this.accountService.register(email, firstName, lastName, password, reTypePassword).subscribe((response: any) => {
        console.log(response);
        this.viewType = "login";
        this.router.navigate(["/events"]);
      })
    } else {
      alert("Passwords are different!");
    }
  }
}
