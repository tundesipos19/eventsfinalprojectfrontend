import {Component, Input} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Category} from "../model/category";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent {
  constructor(private httpClient: HttpClient, private router: Router) {
  }

  @Input()
  category: Category = {
    id: null,
    name: "",
    description: "",
    events: null
  }
  categoryForm: FormGroup = new FormGroup({
    nameInput: new FormControl(),
    descriptionInput: new FormControl()
  });

  ngOnInit() {
    console.log(this.category);
    this.categoryForm = new FormGroup({
      nameInput: new FormControl(this.category.name),
      descriptionInput: new FormControl(this.category.description)
    });
  }

  saveOrUpdateCategory() {
    this.populateCategoryFromForm();
    if (this.category.id == null) {
      this.httpClient.post('/api/categories', this.category).subscribe(
        (response) => {
          var savedCategory = response as Category;
          this.router.navigate(["/categories/" + savedCategory.id]);
        });
    }
    if (this.category.id != null) {
      this.httpClient.put('/api/categories/' + this.category.id, this.category).subscribe(
        (response) => {
          this.router.navigate(['/categories/' + this.category.id]);
        });
    }
  }
  populateCategoryFromForm() {
    this.category.name = this.categoryForm.value.nameInput;
    this.category.description = this.categoryForm.value.descriptionInput;
  }
}
