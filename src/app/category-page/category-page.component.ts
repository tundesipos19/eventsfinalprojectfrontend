import {Component} from '@angular/core';
import {Category} from "../model/category";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationDialogComponent} from "../confirmation-dialog/confirmation-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-category.ts-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.css']
})
export class CategoryPageComponent {
  category: Category = new Category(
    1,
    "Business Meetings, and Conferences",
    "These includes business-related meetings and conferences. It can be a meeting between the shareholders, employees, meeting for product launch, etc.\n",
    null);
  updateCategoryUrl: string = "";

  httpClient: HttpClient;
  route: ActivatedRoute;

  constructor(httpClient: HttpClient, route: ActivatedRoute, private router: Router, private dialog: MatDialog) {
    this.httpClient = httpClient;
    this.route = route;
  }

  ngOnInit() {
    const categoryId = this.route.snapshot.paramMap.get("id");
    this.updateCategoryUrl = "/update-category/" + categoryId;
    this.httpClient.get("/api/categories/" + categoryId).subscribe(
      (response) => {
        console.log(response);
        this.category = response as Category;
      },
      (error) => {
        console.log(error);
        if (error.error == "There is no category with Id: " + categoryId) {
          this.router.navigate(["not-found"])
        }
      });
  }

  public openConfirmationDialog() {
    var dialogReference = this.dialog.open(ConfirmationDialogComponent);
    dialogReference.componentInstance.actionConfirmedEvent.subscribe(actionConfirmed => {
      if (actionConfirmed === true) {
        this.httpClient.delete("/api/categories/" + this.category.id).subscribe(
          success => {
            this.router.navigate(["/categories"]);
          },
          error => {
            alert("Something went wrong!");
            console.log("error" + error)
          }
        )
      }
    })
  }
}
