import {Event} from "./event";

export class Category {
  id: number | null;
  name: string | null;
  description: string | null;
  events: Event[] | null;

  constructor(id: number | null, name: string | null,description: string | null, events: Event[] | null) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.events = events;
  }
}
