export class Account {
  id: number | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  password: string | null;
  reTypePassword: string | null;

  constructor(id: number | null, firstName: string | null, lastName: string | null, email: string | null, password: string | null, reTypePassword: string | null) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.reTypePassword = reTypePassword;
  }
}
