import { Component } from '@angular/core';
import {Account} from "../model/account";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-accounts-page',
  templateUrl: './accounts-page.component.html',
  styleUrls: ['./accounts-page.component.css']
})
export class AccountsPageComponent {
  accounts: Account[] = [];

  constructor(private httpClient: HttpClient,private router: Router) {
  }

  ngOnInit(){
    this.httpClient.get("/api/accounts").subscribe(
      (response) => {
        console.log(response);
        this.accounts = response as Account[];
      }
    );
  }
  navigateToAccount(a: Account){
    this.router.navigate(["/accounts/" + a.id]);
  }

}
