import {Component} from '@angular/core';
import {Account} from "../model/account";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmationDialogComponent} from "../confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.css']
})
export class AccountPageComponent {
  account: Account = new Account(
    1,
    "Sipos ",
    "Tunde",
    "test@gmail.com",
    "123Ab_C",
    "123Ab_C"
  );
  updateAccountUrl: string = "";

  constructor(private httpClient: HttpClient, private activatedRoute: ActivatedRoute, private router: Router, private dialog: MatDialog) {
  }

  ngOnInit() {
    const accountId = this.activatedRoute.snapshot.paramMap.get("id");
    this.updateAccountUrl = "/update-account/" + accountId;
    this.httpClient.get("/api/accounts/" + accountId).subscribe(
      (response) => {
        console.log(response)
        this.account = response as Account;
      },
      (error) => {
        console.log(error);
        if (error.error == "There is no account with id: " + accountId) {
          console.log("There is no account with id: " + accountId);
        }
      }
    );
  }

  public openConfirmationDialog() {
    var dialogReference = this.dialog.open(ConfirmationDialogComponent);
    dialogReference.componentInstance.actionConfirmedEvent.subscribe(actionConfirmed => {
      if (actionConfirmed === true) {
        this.httpClient.delete("/api/accounts/" + this.account.id).subscribe(
          success => {
            this.router.navigate(["/accounts"]);
          },
          error => {
            alert("Something went wrong!")
          }
        )
      }
    })
  }
}
